import { series } from "gulp";
import { build } from "./build";
import { runTests, runTestCoverage } from "../test";

export const test = series(build, runTests);

export const cover = series(build, runTestCoverage);

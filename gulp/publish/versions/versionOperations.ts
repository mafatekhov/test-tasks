import { Version } from "./version";

export function versionHigher(v1: Version, v2: Version): boolean {
  if (v1.major > v2.major) { return true; }
  if (v1.major < v2.major) { return false; }
  if (v1.minor > v2.minor) { return true; }
  if (v1.minor < v2.minor) { return false; }
  return v1.patch > v2.patch;
}

export function incrementVersion(version: Version): Version {
  return {
    major: version.major,
    minor: version.minor,
    patch: version.patch + 1
  };
}

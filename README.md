To participate:\
  1 fork this repository\
  2 implement methods for as much tasks as you can\
  3 create merge request back to original repository.

Solution contains a couple tests for each task.

# Task1
Distinct by Id\
Source collection can have the same object present several times. Implement a method that produces 
a collection without objects with repeating Id.\
File to implement the task => ./src/distinctById/distinctById.ts

# Task2
Predefined order\
Source collection is unordered. Implement a method to output source collection in specified order.\
File to implement the task => ./src/predefinedOrder/orderByPredefinedColor.ts

# Task3
Tree structure\
Source is a collection of items with Id and ParentId fields, where ParentId points at parent item. 
Implement a method that transforms this plain structure into tree structure where parent entity 
has a list of child entities.\
File to implement the task => ./src/createTree/createTree.ts

# Task4
Create pairs\
Source is two arrays of similar objects. Object from first array can have a pair in second array - object that has equal id.
Task is to create array containing all pairs.\
File to implement the task => ./src/createPairs/createPairs.ts



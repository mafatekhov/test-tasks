import { DistinctEntity } from "./entity.type";

/// <summary>
/// returns only objects with unique ids, dropping objects,
///  which ids are already in the output sequence
/// </summary>
/// <param name="entities">input collection</param>
/// <returns>distinct output collection</returns>
interface EntityMap<type> {
  key?: type;
}

export function distinctById(entities: DistinctEntity[]): DistinctEntity[] {
  const uniqueElementsMap: EntityMap<DistinctEntity> = {};
  for (let entity of entities) {
    if (!uniqueElementsMap[entity.id]) {
      uniqueElementsMap[entity.id] = entity;
    }
  }
  return Object.values(uniqueElementsMap);
}


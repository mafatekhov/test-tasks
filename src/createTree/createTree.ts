import { DalEntity } from "./dalEntity.type";
import { BlEntity } from "./blEntity.type";

export function createTree(dal: DalEntity[]): BlEntity[] {
  let map = {};
  let roots = [];

  dal.forEach(({ id, text }: DalEntity) => {
    map[id] = { id, text, children: [] } as BlEntity;
  });

  for (let el of dal) {
    const curNode = map[el.id];
    if (el.parentId) {
      map[el.parentId].children.push(curNode);
    } else {
      roots.push(curNode);
    }
  }
  return roots;
}

export enum Color {
  Black,
  Blue,
  White,
  Green,
  Yellow,
  Orange,
  Red,
  Pink,
  Brown
}


export const enumValues = Object.keys(Color).filter(n => !Number.isNaN(Number(n)));

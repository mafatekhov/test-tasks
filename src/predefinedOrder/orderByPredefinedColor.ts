import { ColoredEntity } from "./entity.type";
import { Color } from "./color.type";

/// <summary>
/// returns items from input collection in order, specified by "predefined" collection
/// </summary>
/// <param name="entities">input collection</param>
/// <param name="order">defines order</param>
/// <returns></returns>
export function orderByPredefinedColor(entities: ColoredEntity[], order: Color[]): ColoredEntity[] {
  return entities.sort((a: ColoredEntity, b: ColoredEntity) => {
    return (order.indexOf(a.color) < 0 || order.indexOf(b.color) < 0) ? order.indexOf(b.color) - order.indexOf(a.color) :
        order.indexOf(a.color) - order.indexOf(b.color);
  });
}

import { SingleEntity, PairEntity } from "./entities.type";

export function createPairs(left: SingleEntity[], right: SingleEntity[]): PairEntity[] {
  let leftEntitiesMap = {};
  let res = [];

  for (let lEntity of left) {
    leftEntitiesMap[lEntity.id] = lEntity;
  }

  right.forEach((rightEntity: SingleEntity) => {
    if (leftEntitiesMap[rightEntity.id]) {
      res.push({left: leftEntitiesMap[rightEntity.id], right: rightEntity});
    }
  });
  return res;
}
